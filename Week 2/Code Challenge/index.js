const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter( (i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  data = clean(data);
  let i, j, temp;
  for (i = 0; i < data.length; i++) {
    let swapped = false;
    for (j = 0; j < data.length - i - 1; j++) {
      if (data[j] > data[j + 1]) {
        // swap the elements
        //temp = data[j];
        [ data[j] , data[j+1] ] = [ data[j + 1] , data[j] ];
        // data[j + 1] = temp;
        swapped = true;
      }
    }
    if (!swapped) {
      return data;
    }
  }
  console.log(data);
  return data;
}

// Should return array
function sortDecending(data) {
  // Code Here
  data = clean(data);
  let i, j, temp;
  let swapped = false;
  for (i = 0; i < data.length; i++) {
    for (j = 0; j < data.length - i - 1; j++) {
      if (data[j] < data[j + 1]) {
        // swap the elements
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
        swapped = true;
      }
    }
    if (!swapped) {
      return data;
    }
  }
  return data;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
