const index = require("../index"); // Import index to run rl on this file

// Function to calculate cube volume
function calculateRectangularPyramid(length,height) {
  return Math.pow(length, 2) * height / 3;
}

// Function to input the length
function input() {
  index.rl.question("Input Length (m): ", (l) => {
    if (!isNaN(l) && !index.isEmptyOrSpaces(l)) {
      index.rl.question("Input Height (m):",(h)=>{
        if (!isNaN(h) && !index.isEmptyOrSpaces(h)) {
          console.log(`\nPyramid's volume is ${calculateRectangularPyramid(l,h)} m3\n`);
          index.menu();
        } else {
          console.log(`Height must be number`);
          input();
        }
      })
    } else {
      console.log(`Length must be number`);
      input();
    }
  });
}

module.exports = { input }; // Export the input, so the another file can run this code
