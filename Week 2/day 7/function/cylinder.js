const index = require("../index");

// Function to calculate cube volume
function calculateCylinder(radius, height) {
  return Math.PI * height * Math.pow(radius, 2);
}

// Function to input the length
function input() {
  index.rl.question("Input Radius (m): ", (r) => {
    if (!isNaN(r) && !index.isEmptyOrSpaces(r)) {
      index.rl.question("Input Height (m):",(h)=>{
        if (!isNaN(h) && !index.isEmptyOrSpaces(h)) {
          console.log(`\nCylinder's volume is ${calculateCylinder(r,h)} m3 \n`);
          index.menu();
        } else {
          console.log(`Height must be number`);
          input();
        }
      })
    } else {
      console.log(`Radius must be number`);
      input();
    }
  });
}

module.exports = { input };
