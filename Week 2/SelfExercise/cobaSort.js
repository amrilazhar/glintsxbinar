let arr = [10,2,1,5,8,8];
let counter = 0;
for (let i = 0; i < arr.length-1;i++){
  console.log(`Looping I ke-`+i);
  for (let j = i+1; j < arr.length; j++){
    console.log(`Looping j ke-`+j);
    console.log("Array pertama : "+arr[i]);
    console.log("Array kedua : "+arr[j]);
    let tmp;
    counter++;
    if(arr[i] > arr[j]) {
      tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;
      console.log("Pindah array : "+arr[i]+" dengan "+arr[j]);
      console.log(arr);
      console.log("============ looping j ===============\n");
    } else {
      console.log("tidak tuker array");
      console.log(arr);
      console.log("============ looping j ===============\n");
    }
  }
  console.log("++++++++++++++++++ looping i +++++++++++++++\n");
}

function bubbleSort(){
  let arrF = [10,2,1,5,8,8];
  let i, j, temp;
  counter = 0;
    for(i = 0; i < arrF.length; i++)
    {
        for(j = 0; j < arrF.length-i-1; j++)
        {
            counter++;
            if( arrF[j] > arrF[j+1])
            {
                // swap the elements
                temp = arrF[j];
                arrF[j] = arrF[j+1];
                arrF[j+1] = temp;
            }
        }
    }
    return arrF;
}

console.log(arr);
console.log("looping sebanyak : "+counter+" kali");
console.log("================= bubbleSort=========================");
console.log(bubbleSort());
console.log("looping sebanyak : "+counter+" kali");

// looping sebanyak (n-1)+(n-2)+(n-3)
