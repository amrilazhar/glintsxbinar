//============================== Import readline =============================//
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

//STRUKTUR fungsi (parameter, parameter)
// parameter bisa berisi fungsi lagi
rl.question("Pertanyaan pertama :", function (pertama) { funtionPertama(pertama); } );

function funtionPertama(pertama) {
  console.log(`Fungsi ke-${pertama}`);

  rl.question("Pertanyaan Kedua :", function (kedua) {
    functionKedua(kedua);
  });
}

function functionKedua(kedua) {
  console.log(`Fungsi ke-${kedua}`);
  rl.question("Pertanyaan Ketiga :", function (ketiga) {
    functionKetiga(ketiga);
  });
}

function functionKetiga(ketiga) {
  console.log(`Fungsi ke-${ketiga}`);
  rl.close();
}
