//============================== Import readline =============================//
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

//============================= input for CUBE ===============================//
// Function for inputing length of Cube
function inputLength() {
  rl.question(`Length: `, (length) => {
    if (!isNaN(length)) {
      if (length !== "") {
        console.log(`\nCube: ${calculateCubeVolume(length)}`);
        console.log("=====================================\n");
        measureGeometry();
      } else {
        console.log(`Length is empty\n`);
        inputLength();
      }
    } else {
      console.log(`Length must be a number\n`);
      inputLength();
    }
  });
}
//============================== Input for TUBE ==============================//
// Function for inputing radius of Tube
function inputRadius() {
  rl.question(`Radius: `, (radius) => {
    if (!isNaN(radius) && radius !== "") {
      inputHeight(radius);
    } else {
      console.log(`Radius must be a number and Not Empty\n`);
      inputRadius();
    }
  });
}

// Function for inputing height of Tube
function inputHeight(radius) {
  rl.question(`Height: `, (height) => {
    if (!isNaN(height) && height !== "") {
      console.log(`\nTube: ${calculateTubeVolume(radius, height)}`);
      console.log("=====================================\n");
      measureGeometry();
    } else {
      console.log(`Height must be a number and Not Empty\n`);
      inputHeight(radius);
    }
  });
}
//============================================================================//

//=============================== CALCULATION FUNCTION =======================//
function calculateTubeVolume(r, h) {
  let circleArea = Math.PI * r ** 2;
  let volume = circleArea * h;
  return volume;
}

function calculateCubeVolume(l) {
  let volume = l ** 3;
  return volume;
}

//=============================== MAIN MENU ==================================//
function measureGeometry() {
  console.log("Choose the Option Number :");
  console.log("1. Tube\n2. Cube\n3. Exit");
  rl.question(`Option: `, (opt) => {
    if (opt == 1) {
      inputRadius();
    } else if (opt == 2) {
      inputLength();
    } else if (opt == 3) {
      rl.close();
    } else {
      console.log(`Your Selection is Not in the OPTION\n`);
      measureGeometry();
    }
  });
}

// call the Main Menu
measureGeometry();
