//const host = require('./resolv')();
const mysql = require("mysql"); // Import mysql

// Make mysql connection
const connection = mysql.createConnection({
  host: "localhost", //host.nameserver[0],
  user: "root",
  password: "root123", //"1234",
  database: "penjualan",
});

const promConnect = (sql,inputArr=null) =>
  new Promise ((fulfill,reject) => {
    connection.query(sql, inputArr, function (err, result) {
      if (err) return reject(err)
      else return fulfill(result)
    })//end connection query
  }
); // end promise

module.exports.connection = connection; // export connection
module.exports.promConnect = promConnect; // export promConnect
