const express = require("express");
const router = express.Router();
const pelangganController = require("../controllers/pelangganController");

router.get("/:id", pelangganController.getOne);
router.get("/", pelangganController.getAll);
router.post('/create', pelangganController.post);
router.put('/update/:id', pelangganController.put);
router.delete('/delete/:id', pelangganController.delete);

module.exports = router;
