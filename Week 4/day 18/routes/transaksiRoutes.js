const express = require("express");
const router = express.Router();
const transaksiController = require("../controllers/transaksiController");

router.get("/read", transaksiController.getAll);
router.post('/create', transaksiController.post);
router.put('/update/:id', transaksiController.put);
router.delete('/delete/:id', transaksiController.delete);
router.get("/read/:id", transaksiController.getOne);

module.exports = router;
