const express = require("express");
const router = express.Router();
const barangController = require("../controllers/barangController");

router.get("/read/:id", barangController.getOne);
router.get("/read", barangController.getAll);
router.post('/create', barangController.post);
router.put('/update/:id', barangController.put);
router.delete('/delete/:id', barangController.delete);

module.exports = router;
