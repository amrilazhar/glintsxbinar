const express = require("express");
const router = express.Router();
const pemasokController = require("../controllers/pemasokController");

router.get("/read/:id", pemasokController.getOne);
router.get("/read", pemasokController.getAll);
router.post('/create', pemasokController.post);
router.put('/update/:id', pemasokController.put);
router.delete('/delete/:id', pemasokController.delete);

module.exports = router;
