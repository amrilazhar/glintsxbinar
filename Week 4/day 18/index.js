const express = require("express");
const app = express();

//import Routes
const barangRoutes = require("./routes/barangRoutes");
const transaksiRoutes = require("./routes/transaksiRoutes");
const pelangganRoutes = require("./routes/pelangganRoutes");
const pemasokRoutes = require("./routes/pemasokRoutes");

const bodyParser = require('body-parser')
// parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//start server
app.listen(3000, () => console.log("server running on port 3000"));

//routes
app.use('/barang', barangRoutes);
app.use('/transaksi', transaksiRoutes);
app.use('/pelanggan', pelangganRoutes);
app.use('/pemasok', pemasokRoutes);
