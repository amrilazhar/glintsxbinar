const connection = require("../models/connection.js").connection;
const promConnect = require("../models/connection.js").promConnect;

class BarangController {
  async post(req, res) {
    try {
      let sql = `INSERT INTO barang(nama,harga,id_pemasok) VALUES ("?",?,?)`;
      let inputArr = [req.body.nama,req.body.harga,req.body.id_pemasok];
      let queryPost = await promConnect(sql, inputArr); //insert data

      if (queryPost) {
        let slqDataInserted = "SELECT * FROM barang WHERE id=?";
        let queryInsertedData = await promConnect(slqDataInserted, [
          queryPost.insertId,
        ]);
        if (queryInsertedData) {
          res.status(201).json({
            status: "success",
            data: queryInsertedData,
          });
        } else {
          res.status(500).json({
            status: "Error sql",
            error: queryInsertedData,
          }); //end res status
        }
      } else {
        res.status(500).json({
          status: "Error sql",
          error: queryPost,
        }); //end res status
      }
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

  async put(req, res) {
    try {
      let sql = `UPDATE penjualan.barang SET nama="?", harga=?, id_pemasok=? WHERE id=?;`;
      let inputArr = [req.body.nama,req.body.harga,req.body.id_pemasok,req.params.id];
      let queryPut = await promConnect(sql, inputArr); //update data

      if (queryPut) {
        let slqDataUpdated = "SELECT * FROM barang WHERE id=?";
        let queryUpdatedData = await promConnect(slqDataUpdated, [
          req.params.id,
        ]);
        if (queryUpdatedData) {
          res.status(201).json({
            status: "success",
            data: queryUpdatedData,
          });
        } else {
          res.status(500).json({
            status: "Error sql",
            error: queryUpdatedData,
          }); //end res status
        }
      } else {
        res.status(500).json({
          status: "Error sql",
          error: queryPut,
        }); //end res status
      }
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

  async delete(req, res) {
    try {
      let sql = `DELETE FROM barang WHERE id = ?`;
      connection.query(sql, [req.params.id], function (err, result) {
        if (err) {
          res.status(204).json({
            status: "Error",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status: "success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(204).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

  async getAll(req, res) {
    try {
      let sql = "SELECT * FROM barang";
      connection.query(sql, function (err, result) {
        if (err) {
          res.status(500).json({
            status: "Error",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status:
            result.affectedRows == 0 ? "Error, id tidak match" : "success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

  async getOne(req, res) {
    try {
      let sql = `SELECT * FROM barang WHERE id = ?`;
      connection.query(sql, [req.params.id], function (err, result) {
        if (err) {
          res.status(500).json({
            status: "Error",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status:"success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

}// end Class

module.exports = new BarangController();
