const connection = require("../models/connection.js").connection;
const promConnect = require("../models/connection.js").promConnect;

class TransaksiController {
  async post(req, res) {
    try {
      //find harga barang
      let inputArrBrg = [req.body.id_barang];
      let queryFindHarga = await promConnect(
        "SELECT harga FROM barang WHERE id=?",
        [req.body.id_barang]
      );
      let totalHarga = eval(queryFindHarga[0].harga) * eval(req.body.jumlah);

      //insert transaksi
      let sql = `INSERT INTO transaksi(id_barang,id_pelanggan,jumlah,total) VALUES (? , ? , ? , ?)`;
      let inputArrTrans = [
        req.body.id_barang,
        req.body.id_pelanggan,
        req.body.jumlah,
        totalHarga,
      ];
      let queryPostTrans = await promConnect(sql, inputArrTrans); //insert data

      //get data that had been inserted
      if (queryPostTrans) {
        let slqDataInserted =
          "SELECT t.id, b.nama as barang, b.harga as harga_satuan, p.nama as pelanggan, t.waktu, t.jumlah, t.total, pe.nama as pemasok FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id JOIN pemasok pe ON b.id_pemasok = pe.id WHERE t.id=? ORDER BY t.id";
        let queryInsertedData = await promConnect(slqDataInserted, [
          queryPostTrans.insertId,
        ]);
        if (queryInsertedData) {
          res.status(201).json({
            status: "success",
            data: queryInsertedData,
          });
        } else {
          res.status(500).json({
            status: "Error sql",
            error: queryInsertedData,
          }); //end res status
        }
      } else {
        res.status(500).json({
          status: "Error sql",
          error: queryPostTrans,
        }); //end res status
      }
    } catch (exception) {
      // If error will be send Error JSON
      res.status(500).json({
        status: "Code Error Bro",
        error: exception,
      });
      console.log(exception);
    }
  }

  async put(req, res) {
    try {
      //find harga barang
      let inputArrBrg = [req.body.id_barang];
      let queryFindHarga = await promConnect(
        "SELECT harga FROM barang WHERE id=?",
        [req.body.id_barang]
      );
      let totalHarga = eval(queryFindHarga[0].harga) * eval(req.body.jumlah);

      //insert transaksi
      let sql = `UPDATE penjualan.transaksi SET id_barang=?, id_pelanggan=?, waktu = NOW(), jumlah=?, total=? WHERE id=?`;
      let inputArrTrans = [
        req.body.id_barang,
        req.body.id_pelanggan,
        req.body.jumlah,
        totalHarga,
        req.params.id,
      ];
      let queryPutTrans = await promConnect(sql, inputArrTrans); //insert data

      //get data that had been inserted
      if (queryPutTrans) {
        let slqDataUpdated =
          "SELECT t.id, b.nama as barang, b.harga as harga_satuan, p.nama as pelanggan, t.waktu, t.jumlah, t.total, pe.nama as pemasok FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id JOIN pemasok pe ON b.id_pemasok = pe.id WHERE t.id=? ORDER BY t.id";
        let queryUpdatedData = await promConnect(slqDataUpdated, [
          req.params.id,
        ]);
        if (queryUpdatedData) {
          res.status(201).json({
            status: "success",
            data: queryUpdatedData[0],
          });
        } else {
          res.status(500).json({
            status: "Error sql",
            error: queryUpdatedData,
          }); //end res status
        }
      } else {
        res.status(500).json({
          status: "Error sql",
          error: queryPutTrans,
        }); //end res status
      }
    } catch (exception) {
      // If error will be send Error JSON
      res.status(500).json({
        status: "Code Error Bro",
        error: exception,
      });
      console.error(exception);
    }
  }

  async delete(req, res) {
    try {
      let sql = `DELETE FROM transaksi t WHERE id = ?`;
      connection.query(sql, [req.params.id], function (err, result) {
        if (err) {
          res.status(204).json({
            status:
              result.affectedRows == 0 ? "Error, id tidak match" : "success",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status:
            result.affectedRows == 0 ? "Error, id tidak match" : "success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(204).json({
        status: "Code Error Bro",
        error: exception,
      });
      console.log(exception);
    }
  }

  async getAll(req, res) {
    try {
      let sql =
        "SELECT t.id, b.nama as barang, b.harga as harga_satuan, p.nama as pelanggan, t.waktu, t.jumlah, t.total, pe.nama as pemasok FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id JOIN pemasok pe ON b.id_pemasok = pe.id ORDER BY t.id;";
      connection.query(sql, function (err, result) {
        if (err) {
          res.status(500).json({
            status: "Error",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status: "success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception,
      });
      console.log(exception);
    }
  }

  async getOne(req, res) {
    try {
      let sql =
        "SELECT t.id, b.nama as barang, b.harga as harga_satuan, p.nama as pelanggan, t.waktu, t.jumlah, t.total, pe.nama as pemasok FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id JOIN pemasok pe ON b.id_pemasok = pe.id WHERE t.id=?;";
      connection.query(sql, [req.params.id],function (err, result) {
        if (err) {
          res.status(500).json({
            status: "Error",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status: "success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception,
      });
      console.log(exception);
    }
  }

}//end Class

module.exports = new TransaksiController();
