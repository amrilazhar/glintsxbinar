const connection = require("../models/connection.js").connection;
const promConnect = require("../models/connection.js").promConnect;

class PemasokController {
  async post(req, res) {
    try {
      let sql = `INSERT INTO pemasok(nama) VALUES ("?")`;
      let inputArr = [req.body.nama];
      let queryPost = await promConnect(sql, inputArr); //insert data

      if (queryPost) {
        let slqDataInserted = "SELECT * FROM pemasok WHERE id=?";
        let queryInsertedData = await promConnect(slqDataInserted, [queryPost.insertId,]);
        if (queryInsertedData) {
          res.status(201).json({
            status: "success",
            data: queryInsertedData,
          });
        } else {
          res.status(500).json({
            status: "Error sql",
            error: queryInsertedData,
          }); //end res status
        }
      } else {
        res.status(500).json({
          status: "Error sql",
          error: queryPostTrans,
        }); //end res status
      }
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

  async put(req, res) {
    try {
      let sql = `UPDATE penjualan.pemasok SET nama="?" WHERE id=?;`;
      let inputArr = [req.body.nama,req.params.id,];
      let queryPut = await promConnect(sql, inputArr); //insert data

      if (queryPut) {
        let slqDataUpdated = "SELECT * FROM pemasok WHERE id=?";
        let queryUpdatedData = await promConnect(slqDataUpdated, [req.params.id,]);
        if (queryUpdatedData) {
          res.status(201).json({
            status: "success",
            data: queryUpdatedData,
          });
        } else {
          res.status(500).json({
            status: "Error sql",
            error: queryUpdatedData,
          }); //end res status
        }
      } else {
        res.status(500).json({
          status: "Error sql",
          error: queryPut,
        }); //end res status
      }
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

  async delete(req, res) {
    try {
      let sql = `DELETE FROM pemasok WHERE id = ?`;
      connection.query(sql, [req.params.id], function (err, result) {
        if (err) {
          res.status(204).json({
            status: "Error",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status:
            result.affectedRows == 0 ? "Error, id tidak match" : "success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(204).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

  async getAll(req, res) {
    try {
      let sql = "SELECT * FROM pemasok";
      connection.query(sql, function (err, result) {
        if (err) {
          res.status(500).json({
            status: "Error",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status: "success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

  async getOne(req, res) {
    try {
      let sql = "SELECT * FROM pemasok WHERE id=?";
      connection.query(sql, [req.params.id],function (err, result) {
        if (err) {
          res.status(500).json({
            status: "Error",
            error: err,
          });
        } // If error

        // If success it will return JSON of result
        res.status(200).json({
          status: "success",
          data: result,
        });
      });
    } catch (exception) {
      res.status(500).json({
        status: "Code Error Bro",
        error: exception
      });
      console.log(exception);
    }
  }

}//end class

module.exports = new PemasokController();
