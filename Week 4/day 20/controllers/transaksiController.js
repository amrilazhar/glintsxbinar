const { transaksi, barang, pelanggan, pemasok } = require("../models");
const {
  check,
  validationResult,
  matchedData,
  sanitize,
} = require("express-validator"); //form validation & sanitize form params

class TransaksiController {
  // Get All data from transaksi
  async getAll(req, res) {
    transaksi
      .findAll({
        // find all data of Transaksi table
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]], // just these attributes that showed
        include: [
          {
            model: barang,
            attributes: ["nama"], // just this attrubute from Barang that showed
            include: [
              {
                model: pemasok,
                attributes: ["nama"],
              },
            ],
          },
          {
            model: pelanggan,
            attributes: ["nama"], // just this attrubute from Pelanggan that showed
          },
        ],
      })
      .then((transaksi) => {
        res.status(200).json({ message: "success", data: transaksi }); // Send response JSON and get all of Transaksi table
      })
      .catch((err) => res.json(err));
  } // end get ALL

  // Get All data from transaksi
  async getOne(req, res) {
    transaksi
      .findOne({
        // find all data of Transaksi table
        where: { id: `${req.params.id}` },
        //attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]], // attributes ga usah di pakai klo mau select ALL
        include: [
          {
            model: barang,
            attributes: ["nama"], // just this attrubute from Barang that showed
            include: [
              {
                model: pemasok,
                attributes: ["nama"],
              },
            ],
          },
          {
            model: pelanggan,
            attributes: ["nama"], // just this attrubute from Pelanggan that showed
          },
        ],
      })
      .then((transaksi) => {
        res.status(200).json({ message: "success", data: transaksi }); // Send response JSON and get all of Transaksi table
      })
      .catch((err) => res.json(err));
  } // end get Ones

  // create data from transaksi
  async create(req, res) {
    barang
      .findOne({
        where: { id: `${req.body.id_barang}` },
        attributes: ["harga"],
      })
      .then((items) => {
        if (!items) {
          return res.status(500).json({message:`barang dengan id = ${req.body.id_barang} belum terdaftar`});
        }
        let totalHarga = eval(items.harga) * eval(req.body.jumlah);
        return transaksi.create({
          id_barang: req.body.id_barang,
          id_pelanggan: req.body.id_pelanggan,
          jumlah: req.body.jumlah,
          total: totalHarga,
        });
      })
      .then((newtransaksi) => {
        let date = new Date();
        newtransaksi.dataValues.updatedAt = new Date(
          newtransaksi.dataValues.updatedAt - date.getTimezoneOffset() * 60000
        );
        newtransaksi.dataValues.createdAt = new Date(
          newtransaksi.dataValues.createdAt - date.getTimezoneOffset() * 60000
        );

        res.status(200).json({ message: "success", data: newtransaksi }); // Send response JSON and get all of Transaksi table
      })
      .catch((err) => res.json(err));
  } // end Insert

  // update data from transaksi
  async update(req, res) {
    try {
      let findBarang = await barang.findOne({
        where: { id: req.body.id_barang },
      });

      if (!findBarang) {
        return res.status(500).json({message:`barang dengan id = ${req.body.id_barang} belum terdaftar`});
      }
      console.log("success findbarang");
      let totalHarga = eval(findBarang.harga) * req.body.jumlah;
      let updateAttr = {
        id_barang: req.body.id_barang,
        id_pelanggan: req.body.id_pelanggan,
        jumlah: req.body.jumlah,
        total: totalHarga,
      };
      let updateTrans = await transaksi.update(updateAttr, {
        where: { id: req.params.id },
      });

      if (updateTrans[0]===0) {
        return res.status(500).json({message:"transaksi belum terdaftar"});
      }
      //menampilkan data yang diupdate
      let findTransOne = await transaksi.findOne({
        where: { id: req.params.id },
      });
      //change date to local timezone
      let date = new Date();
      findTransOne.dataValues.updatedAt = new Date(
        findTransOne.dataValues.updatedAt - date.getTimezoneOffset() * 60000
      );
      findTransOne.dataValues.createdAt = new Date(
        findTransOne.dataValues.createdAt - date.getTimezoneOffset() * 60000
      );

      return res.status(200).json({
        message : "success",
        data : findTransOne,
      });

    } catch (e) {
      res.status(500).json(e);
    }

  } // end Update
}

module.exports = new TransaksiController();
