"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("pelanggan", [
      {
        nama: "Jhorgi",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Dedi",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Kamal",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("pelanggan", null, {});
  },
};
