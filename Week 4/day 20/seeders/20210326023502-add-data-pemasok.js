"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("pemasok", [
      {
        nama: "Karina",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Risa",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Azhar",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("pemasok", null, {});
  },
};
