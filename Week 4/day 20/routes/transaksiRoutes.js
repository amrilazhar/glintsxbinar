const express = require("express"); // Import express
const router = express.Router(); // Make router from app
const transaksiController = require("../controllers/transaksiController.js"); // Import TransaksiController
//const transaksiValidator = require("../middlewares/validators/transaksiValidator.js"); // Import validator to validate every request from user

router.get("/", transaksiController.getAll); // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get("/:id", transaksiController.getOne); // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.post('/', /*transaksiValidator.create,*/ transaksiController.create) // If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.put('/:id', /*transaksiValidator.update,*/ transaksiController.update) // If accessing localhost:3000/transaksi/update/:id, it will call update function in transaksiController class
//router.delete('/delete/:id', transaksiController.delete) // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
