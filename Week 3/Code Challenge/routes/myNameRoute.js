const express = require("express");
const router = express.Router();
const myNameController = require("../controllers/myNameController");

router.get("/:name", myNameController.get);
router.post('/:name', myNameController.post);
router.put('/:name', myNameController.put);
router.delete('/:name', myNameController.delete);

module.exports = router;
