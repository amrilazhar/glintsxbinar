const express = require("express");
const app = express();
const helloRoutes = require("./routes/myNameRoute");

app.listen(3000, () => console.log("server running on port 3000"));
app.use('/', helloRoutes);
