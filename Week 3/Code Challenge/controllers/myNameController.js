class MyNameController {
  async post(req, res) {
    try {
      console.log("send post");
      if (req.params.name === "amril") {
        res.send(`Amril Azhar`);
      } else {
        res.send(`Wrong Name!`);
      }
    } catch (exception) {
      res.status(500).send(exception);
    }
  }

  async get(req, res) {
    try {
      console.log("send get");
      if (req.params.name === "amril") {
        res.send(`Amril Azhar`);
      } else {
        res.send(`Wrong Name get!`);
      }
    } catch (exception) {
      res.status(500).send(exception);
    }
  }

  async put(req, res) {
      try {
        console.log("send put");
        if (req.params.name === "amril") {
          res.send(`Amril Azhar`);
        } else {
          res.send(`Wrong Name!`);
        }
      } catch (exception) {
        res.status(500).send(exception);
      }
    }

  async delete(req, res) {
    try {
      console.log("send delete");
      if (req.params.name === "amril") {
        res.send(`Amril Azhar`);
      } else {
        res.send(`Wrong Name!`);
      }
    } catch (exception) {
      res.status(500).send(exception);
    }
  }

}

module.exports = new MyNameController;
