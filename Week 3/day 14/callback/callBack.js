// Import fs module that can read/write file
const fs = require('fs')


/* Start callback (ES5) */
fs.readFile('./contents/content1.txt', 'utf-8', (err, content) => { // read file content1.txt
  if (err) throw err // error if can't read
  console.log(content);
  fs.readFile('./contents/content2.txt', 'utf-8', (err, content) => { // read file content2.txt
    if (err) throw err // error if can't read
    console.log(content);
    console.log('Writing done!') // print Writing done! if no error
  })
})
/* End callback */
