// Import fs module that can read/write file
const fs = require("fs");

/* Start make promise object */
const readFile = (file, options) =>
  new Promise((fulfill, reject) => {
    fs.readFile(file, options, (err, content) => {
      if (err) return reject(err);
      return fulfill(content);
    });
  });
/* End make promise object */

const readAllFiles = async () => {
  try {
    let arr = [];
    for (var i = 0; i < 10; i++) {
      arr.push(readFile(`./callback/contents/content${i+1}.txt`,"utf-8"))
    }
    let data = await Promise.all(arr);

    console.log(data);

  } catch (e) {
    console.log(e);
    console.log("Penggabungan Kata tidak terjadi");
  }
}

readAllFiles();
