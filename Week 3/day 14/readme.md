# Tugas Javascript ES6

## Example Async Await
![Gif_async Logo](./asyncAwait.gif)

## Example Promise
![Gif_promiseLogo](./promise.gif)

## Example Promise With some Cases
#### with Recursion, and test called manually one by one
![Gif_promiseLogo](./promiseCases.gif)
