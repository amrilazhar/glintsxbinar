// Import fs module that can read/write file
const fs = require("fs");

/* Start make promise object */
const readFile = (file, options) =>
  new Promise((fulfill, reject) => {
    fs.readFile(file, options, (err, content) => {
      if (err) return reject(err);
      return fulfill(content);
    });
  });
/* End make promise object */

readFile(
  "../callback/contents/content2.txt",
  "utf-8",
).then(content=>{
    console.log("sukses read file");
    console.log(content);
}).catch( error =>{
    console.log("Errornya ini Bro : ", error);
});

readFile(
  "../callback/contents/content1.txt",
  "utf-8"
).then(content=>{
    console.log("sukses read file");
    console.log(content);
    return readFile("../callback/contents/content2.txt","utf-8");
}).then(content => {
    console.log("sukses read file ke 2");
    console.log(content);
}).catch( error =>{
    console.log("Errornya ini Bro : ", error);
});
