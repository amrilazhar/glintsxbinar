const xy = (x,y) => {
  return x*y;
};

console.log(xy(10,10));

const hitung = x => y => { return x * y }
console.log(`Hasil hitung = ${hitung(10)(10)}`);


// normal type of currying function
function fx(x){
  return function fy(y) {
    return x*y;
  }
}
console.log(fx(10)(5));

// const app = () => class App {
//   abi(){
//     console.log("abi");
//   }
//   kamal(){
//     console.log("kamal");
//   }
// };
//
// // let appss = new App();
// // appss.kamal();
// console.log(app.App);
