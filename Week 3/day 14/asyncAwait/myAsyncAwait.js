// Import fs module that can read/write file
const fs = require("fs");

/* Start make promise object */
const readFile = (file, options) =>
  new Promise((fulfill, reject) => {
    fs.readFile(file, options, (err, content) => {
      if (err) return reject(err);
      return fulfill(content);
    });
  });
/* End make promise object */

const readAllFiles = async () => {
  try {
    let content1 = await readFile("../callback/contents/content1.txt","utf-8");
    let content2 = await readFile(content1.trimEnd(),"utf-8");

    let data = await Promise.all([
      readFile("../callback/contents/content1.txt","utf-8"),
      readFile("../callback/contents/content2.txt","utf-8")
    ]);


    let content3 = content1+content2;

    console.log(data);
    console.log(content3);

  } catch (e) {
    console.log(e);
    console.log("Penggabungan Kata tidak terjadi");
  }
}

readAllFiles();
