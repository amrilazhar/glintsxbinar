// Import fs module that can read/write file
const fs = require("fs");

/* Start make promise object */
const readFile = (file, options) =>
  new Promise((fulfill, reject) => {
    fs.readFile(file, options, (err, content) => {
      if (err) return reject(err);
      return fulfill(content);
    });
  });
/* End make promise object */

// CARA MANUAL
const readAllFilesManual = ()=>readFile(`./callback/contents/content${i}.txt`, "utf-8")
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    i++;
    arrCont.push(content);
    return readFile(`./callback/contents/content${i}.txt`, "utf-8");
  })
  .then((content) => {
    arrCont.push(content);
    console.log("======Cara Normal/Manual ===========");
    console.log(arrCont);
  })
  .catch((error) => {
    console.log("Errornya ini Bro : ", error);
  });

//test promise pake looping rekursi
const readAllFiles = (start, end) => {
  readFile(`./callback/contents/content${start}.txt`, "utf-8")
    .then((content) => {
      arrCont2.push(content);
      if (start < end) {
        start++;
        return readAllFiles(start, end);
      } else {
        console.log("==========Pake Rekursi========");
        console.log(arrCont2);
      }
    })
    .catch((error) => {
      console.log("Errornya ini Bro : ", error);
    });
};

//// Test Klo manual di panggiil satu2
const readAllFiles1 = ()=>readFile(`./callback/contents/content1.txt`, "utf-8")
  .then((content) => {
    arrCont3.push(content);
  }).catch((error) => {
    console.log("Errornya ini Bro : ", error);
  });
const readAllFiles2 = ()=>readFile(`./callback/contents/content2.txt`, "utf-8")
  .then((content) => {
    arrCont3.push(content);
  }).catch((error) => {
    console.log("Errornya ini Bro : ", error);
  });
const readAllFiles3 = ()=>readFile(`./callback/contents/content3.txt`, "utf-8")
  .then((content) => {
    arrCont3.push(content);
    console.log("========Kalo dipanggil satu-satu ========");
    console.log(arrCont3);
  }).catch((error) => {
    console.log("Errornya ini Bro : ", error);
  });

//test promise pake looping rekursi cara kedua
const readAllFilesV2 = (end, url, start=0, arrCont=[]) => {
  readFile(url[start], "utf-8")
    .then((content) => {
      arrCont.push(content);
      if (start < end-1) {
        start++;
        return readAllFilesV2(end, url,start, arrCont);
      } else {
        console.log("=========Pake Rekursi V2=========");
        console.log(arrCont);
      }
    })
    .catch((error) => {
      console.log("Errornya ini Bro : ", error);
    });
};

////////////// Prepare DATA /////////////////////////
// Cara Standar/MANUAL
let i = 1;
let arrCont = [];

// test pake rekursi
let arrCont2 = [];

// test dipanggil 1 per 1
let arrCont3 = [];

//pake rekursi v2
let arrUrl = [];
for (let x = 0; x < 10; x++) {
  arrUrl.push(`./callback/contents/content${x+1}.txt`);
}
////////////// End Prepare DATA /////////////////////////

//run CODE
readAllFilesManual();
readAllFiles(1,10);
readAllFilesV2(10,arrUrl);

readAllFiles1();
readAllFiles2();
readAllFiles3();
