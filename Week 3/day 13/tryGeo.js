const { Square, Rectangle, Triangle, Tube, Cube, Beam, Cone } = require("./geometry");

let trySquare = new Square(17);
trySquare.introduce("Azhar");
trySquare.calculateArea();

let tryRectangle = new Rectangle(11, 12);
tryRectangle.introduce("Azhar");
tryRectangle.calculateArea();

let tryTriangle = new Triangle(10, 6);
tryTriangle.introduce("Azhar");
tryTriangle.calculateArea();

let tryTube = new Tube(100, 12);
tryTube.introduce("Azhar");
tryTube.calculateVolume();

let tryCube = new Cube(16);
tryCube.introduce("Azhar");
tryCube.calculateVolume();

let tryBeam = new Beam(5,3,4);
tryBeam.introduce("Azhar");
tryBeam.calculateVolume();

let tryCone = new Cone(5,10);
tryCone.introduce("Azhar");
tryCone.calculateVolume();
