const TwoDimention = require("../twoDimention");

class Rectangle extends TwoDimention {
  constructor(length, width) {
    super("Rectangle");
    this.length = length;
    this.width = width;
  }

  // Overloading method
  introduce(who) {
    super.introduce();
    console.log(`${who}, the type of this geometry is ${this.name}`);
    console.log(`Specification : Length = ${this.length} cm | Width = ${this.width} cm`);
  }

  // Overridding
  calculateArea() {
    super.calculateArea();
    let area = this.length * this.width;

    console.log(`This area is ${this.getNumberFormat(area)} cm2 \n`);
  }

  calculateCircumference() {
    super.calculateCircumference();
    let circumference = 2 * (this.length + this.width);

    console.log(`This circumference is ${this.getNumberFormat(circumference)} cm \n`);
  }
}

module.exports = Rectangle;
