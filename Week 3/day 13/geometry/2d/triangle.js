const TwoDimention = require("../twoDimention");

class Rectangle extends TwoDimention {
  constructor(base, height) {
    super("Triangle");
    this.base = base;
    this.height = height;
  }

  // Overloading method
  introduce(who) {
    super.introduce();
    console.log(`${who}, the type of this geometry is ${this.name}`);
    console.log(`Specification : Base = ${this.base} cm | Height = ${this.height} cm`);
  }

  // Overridding
  calculateArea() {
    super.calculateArea();
    let area = this.base * this.height / 2;

    console.log(`This area is ${this.getNumberFormat(area)} cm2 \n`);
  }

  calculateCircumference() {
    super.calculateCircumference();
    let circumference = 3 * this.base;

    console.log(`This circumference is ${this.getNumberFormat(circumference)} cm \n`);
  }
}

module.exports = Rectangle;
