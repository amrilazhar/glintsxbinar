const Square = require("./2d/square");
const Rectangle = require("./2d/rectangle");
const Triangle = require("./2d/triangle");
const Tube = require("./3d/tube");
const Cube = require("./3d/cube");
const Cone = require("./3d/cone");
const Beam = require("./3d/beam");

module.exports = { Square, Rectangle, Triangle, Tube, Cube, Beam, Cone };
