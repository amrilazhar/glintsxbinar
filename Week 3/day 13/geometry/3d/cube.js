const ThreeDimention = require("../threeDimention");

class Cube extends ThreeDimention {
  constructor(width) {
    super("Cube");
    this.width = width;
  }

  // Overloading method
  introduce(who) {
    super.introduce();
    console.log(`${who}, the type of this geometry is ${this.name}`);
    console.log(`Specification : Width = ${this.width} cm`);
  }

  // Overridding
  calculateVolume() {
    super.calculateVolume();
    let vol = this.width**3 ;

    console.log(`This Volume is ${this.getNumberFormat(vol)} cm3 \n`);
  }
}

module.exports = Cube;
