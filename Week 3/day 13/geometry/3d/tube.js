const ThreeDimention = require("../threeDimention");

class Tube extends ThreeDimention {
  constructor(radius,height) {
    super("Tube");
    this.radius = radius;
    this.height = height;
  }

  // Overloading method
  introduce(who) {
    super.introduce();
    console.log(`${who}, the type of this geometry is ${this.name}`);
    console.log(`Specification : Radius = ${this.radius} cm | Height = ${this.height} cm`);
  }

  // Overridding
  calculateVolume() {
    super.calculateVolume();
    let vol = this.getPI() * (this.radius ** 2) * this.height ;

    console.log(`This Volume is ${this.getNumberFormat(vol)} cm3 \n`);
  }
}

module.exports = Tube;
