const ThreeDimention = require("../threeDimention");

class Beam extends ThreeDimention {
  constructor(length,width,height) {
    super("Beam");
    this.width = width;
    this.length = length;
    this.height = height;
  }

  // Overloading method
  introduce(who) {
    super.introduce();
    console.log(`${who}, the type of this geometry is ${this.name}`);
    console.log(`Specification : Length = ${this.length} cm | Width = ${this.width} cm | Height = ${this.height} cm`);
  }

  // Overridding
  calculateVolume() {
    super.calculateVolume();
    let vol = this.width * this.length * this.height ;

    console.log(`This Volume is ${this.getNumberFormat(vol)} cm3 \n`);
  }
}

module.exports = Beam;
