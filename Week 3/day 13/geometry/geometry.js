class Geometry {
  //apply private things
  #numberFormat = new Intl.NumberFormat('es-ES');
  #pi = 3.14;
  constructor(name, type) {
    // Make abstract class
    if (this.constructor == Geometry) {
      throw new Error("Can not make object!");
    }

    this.name = name;
    this.type = type;
  }

  introduce() {
    this.#accessIntroduce();
  }

  // Private that can only accessed in this class
  #accessIntroduce() {
    console.log("Hello, this is Geometry");
  }

  // access private numberformat
  getNumberFormat(number){
    return this.#numberFormat.format(number);
  }
  // access private PI
  getPI(){
    return this.#pi;
  }
}

module.exports = Geometry;
