const host = require('./resolv.js')();
const mysql = require("mysql"); // Import mysql

// Make mysql connection
const connection = mysql.createConnection({
  host: host.nameserver[0],
  user: "ubuntu",
  password: "1234",
  database: "project_dmt",
});

module.exports = connection; // export connection
