const connection = require('../models/connection.js');

class HelloController {

  async post(req, res) {
    try {
      console.log("Hello World Terminal POST");
      res.send(`Hello World POST, nama :  ${req.body.name} and pet kedua : ${req.body.pet[1]} and asal : ${req.body.asal} `);
    } catch (exception) {
      res.status(500).send(exception);
    }
  }

  async get(req, res) {
    try {
      res.send(`Amril Azhar`);
    } catch (exception) {
      res.status(500).send(exception);
    }
  }

  async getParams(req, res) {
    try {
      console.log("Hello World2");
      res.send(`Hello World GET, for ${req.params.nama} from ${req.params.asal}`);
    } catch (exception) {
      res.status(500).send(exception);
    }
  }

  async put(req, res) {
      try {
        console.log("Hello World Terminal PUT");
        res.send("Hello World PUT");
      } catch (exception) {
        res.status(500).send(exception);
      }
    }

  async delete(req, res) {
    try {
      console.log("Hello World Terminal DELETE");
      res.send("Hello World DELETE");
    } catch (exception) {
      res.status(500).send(exception);
    }
  }

  async wrong(req, res) {
    try {
      res.send("Wrong Username");
    } catch (exception) {
      res.status(500).send(exception);
    }
  }

  async getData(req, res) {
    try {
      connection.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
      });
      let sql = "SELECT * FROM dbproj LIMIT 50";
      connection.query(sql, function(err, result) {
        if (err) {
          res.json({
            status: "Error",
            error: err
          });
        } // If error

        // If success it will return JSON of result
        res.json({
            status: "success",
            data: result
          })
        });

    } catch (exception) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      });

    }
  }

}

module.exports = new HelloController;
