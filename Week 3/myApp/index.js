const express = require("express");
const app = express();
const helloRoutes = require("./routes/helloRoutes");
const wrongRoutes = require("./routes/wrongRoute");

const bodyParser = require('body-parser')
// parser
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
app.listen(3000, () => console.log("server running on port 3000"));
app.use('/', helloRoutes);
app.use('/test', wrongRoutes);


// app.get( "/:nama/:asal", (req, res) => {
//   console.log("Hello World2");
//   res.send(`Hello World GET, for ${req.params.nama} from ${req.params.asal}`);
// });
//
// app.get("/", (req, res) => {
//   console.log("Hello World2");
//   res.send(`Hello World GET, for ${req.query.nama} from ${req.query.asal}`);
// });
//
// app.post("/", (req, res) => {
//   console.log("Hello World2");
//   res.send(`Hello World POST, nama :  ${req.body.name} and pet : ${req.body.pet[0]} and asal : ${req.body.asal} `);
// });
//
// app.put("/", (req, res) => {
//   console.log("Hello World2");
//   res.send("Hello World PUT");
// });
//
// app.delete("/", (req, res) => {
//   console.log("Hello World2");
//   res.send("Hello World DELETE");
// });
