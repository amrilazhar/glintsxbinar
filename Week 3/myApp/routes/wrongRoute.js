const express = require("express");
const router = express.Router();
const helloController = require("../controllers/helloControllers");

//router.get("/", helloController.getParams);
router.get("/data", helloController.getData);

module.exports = router;
