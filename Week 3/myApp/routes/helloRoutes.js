const express = require("express");
const router = express.Router();
const helloController = require("../controllers/helloControllers");

//router.get("/", helloController.getParams);
router.get("/amril", helloController.get);
router.post('/amril', helloController.post);
router.put('/amril', helloController.put);
router.delete('/amril', helloController.delete);

//router.use("/*", helloController.wrong);
// router.post('/*', helloController.wrong);
// router.put('/*', helloController.wrong);
// router.delete('/*', helloController.wrong);

module.exports = router;
