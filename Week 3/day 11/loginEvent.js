const EventEmitter = require("events"); // Import
const readline = require("readline");

// Initialize an instance because it is a class
const my = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Registering a listener
my.on("Login Failed", function (email,password) {
  // TODO: Saving the login trial count in the database
  console.log(email, "is failed to login!", "\nthe password : ",password," is wrong");
});
my.on("Login Berhasil", function (email) {
  // TODO: Saving the login trial count in the database
  console.log(email, "is success to login!");
});

const user = {
  login(email, password) {
    const passwordStoredInDatabase = "123456";

    if (password !== passwordStoredInDatabase) {
      my.emit("Login Failed", email, password); // Pass the email to the listener
      ask();
    } else {
      // Do something
      my.emit("Login Berhasil", email);
      rl.close();
    }
  },
};

// readline
let ask = () => {
  rl.question("Email: ", function (email) {
    rl.question("Password: ", function (password) {
      user.login(email, password); // Run login function
    });
  });
};

ask();
