const EventEmitter = require('events'); // Import

 // Initialize an instance because it is a class
const myFriend = new EventEmitter();

// Registering a listener
myFriend.on("Punched", () => {
  console.log("Says : \"Sakit Bro\"");
});

myFriend.on("Dapat Hadiah", () => {
  console.log("Says : \"Thank you\"");
});


myFriend.emit("Dapat Hadiah");
myFriend.emit("Punched");
