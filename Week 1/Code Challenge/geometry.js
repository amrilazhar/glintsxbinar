function calculateTubeVolume(r, t) {
  let circleArea = Math.PI * r ** 2;
  let volume = circleArea * t;
  return volume;
}

function calculateCubeVolume(p,l,t) {
  let volume = p*l*t;
  return volume;
}

let p = 10;
let l = 10;
let t = 3;
let r = 15;

console.log("Tinggi (Cube 1): "+t);
console.log("Jari-jari : "+r);
console.log("Tube Volume : "+ calculateTubeVolume(r, t)+" cm3.");

console.log("Panjang (Cube 1) : "+p);
console.log("Lebar (Cube 1) : "+l);
console.log("Tinggi (Cube 1): "+t);
let cube1 = calculateCubeVolume(p, l, t);
console.log("First Cube Volume : "+cube1+" cm3.");

p = 20;
l = 10;
t = 6;
console.log("Panjang (Cube 2) : "+p);
console.log("Lebar (Cube 2) : "+l);
console.log("Tinggi (Cube 2): "+t);
let cube2 = calculateCubeVolume(p, l, t);
console.log("Cube Volume : "+ cube2+" cm3.");
