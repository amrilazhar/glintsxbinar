const express = require("express");

const transaksiController = require("../controllers/transaksiController")

const router = express.Router();

router.get("/", transaksiController.getAll);
router.get("/:id", transaksiController.getOne);
router.post("/create", transaksiController.create);
router.put("/update/:id", transaksiController.update);

module.exports = router;
