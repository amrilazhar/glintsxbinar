const mongoose = require("mongoose");
const mongoose_delete = require('mongoose-delete');

const PelangganSchema = new mongoose.Schema({
  nama: {
    type: String,
    required: true
  },
  photo :{
    type:String,
    required: false,
    get : getPhoto,
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  toJSON: { getters: true },
});

function getPhoto(image) {
  return image && `/images/${image}`;
}

PelangganSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = pelanggan = mongoose.model('pelanggan', PelangganSchema, 'pelanggan');
