const { ObjectId } = require("mongodb");


class MyController {
  async getStudents(req, res) {
    try {
      const academy = req.app.conMongo.db("academy");
      const students = academy.collection("students"); // Connect to transaksi collection / table
      // find all students data
      let dataStudents = await students.find({}).toArray();
      return res.status(200).json({
        message: "OK get students",
        data: dataStudents ? dataStudents : "No Data",
      });
    } catch (e) {
      console.log(e);
      res.status(500).json(e);
    }
  }

  async getStudent(req, res) {
    try {
      const academy = req.app.conMongo.db("academy");
      const student = academy.collection("students"); // Connect to collection / table
      const classes = academy.collection("class");

      // find data student
      let dataStudent = await student.findOne({
        _id: new ObjectId(req.params.id),
      });

      let dataClass = await classes.findOne({
        _id: new ObjectId(dataStudent.class),
      });
      return res.status(200).json({
        message: "OK get students",
        data: dataStudent ? dataStudent : "No Data",
      });
    } catch (e) {
      console.log(e);
      res.status(500).json(e);
    }
  }

  async getClass(req, res) {
    try {
      const academy = req.app.conMongo.db("academy");
      const classes = academy.collection("class"); // Connect to collection / table
      // find all transaksi data
      let dataClass = await classes.find({}).toArray();
      return res.status(200).json({
        message: "OK get classes",
        data: dataClass ? dataClass : "No Data",
      });
    } catch (e) {
      console.log(e);
      res.status(500).json(e);
    }
  }

  async getOneClass(req, res) {
    try {
      const academy = req.app.conMongo.db("academy");
      const classes = academy.collection("class"); // Connect to collection / table
      // find all transaksi data
      let dataClass = await classes.findOne({
        _id: new ObjectId(req.params.id),
      });
      //console.log(dataClass);
      return res.status(200).json({
        message: "OK get classes",
        data: dataClass ? dataClass : "No Data",
      });
    } catch (e) {
      console.log(e);
      res.status(500).json(e);
    }
  }

  async insertStudent(req, res) {
    try {
      const academy = req.app.conMongo.db("academy");
      const student = academy.collection("students");
      const classes = academy.collection("class");

      // get data class
      let findClass = await classes.findOne({
        _id: new ObjectId(req.body.class_id),
      });
      if (!findClass._id) {
        return res.status(422).json({ message: "object id salah!" });
      }

      // find all students data
      let dataStudent = await student.insertOne({
        nama: req.body.nama,
        city: req.body.city,
        course: req.body.course,
        class: findClass,
      });
      return res.status(200).json({
        message: "OK insert students",
        data: dataStudent.ops[0] ? dataStudent.ops[0] : "No Data",
      });
    } catch (e) {
      console.log(e);
      res.status(500).json(e);
    }
  }

  async deleteStudent(req, res) {
    try {
      const academy = req.app.conMongo.db("academy");
      const student = academy.collection("students");

      let deleteStudent = await student.deleteOne({
        _id: new ObjectId(req.params.id),
      });
      console.log(deleteStudent);
      return res.status(200).json({
        message: "data deleted",
        deleteStudent,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "internal server error",
        error: e,
      });
    }
  }

  async test(req,res){
    console.log(req.body.image);
    return res.send(req.body);
  }
}

module.exports = new MyController();
