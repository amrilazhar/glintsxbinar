const { MongoClient } = require('mongodb') // Import mongodb
const assert = require('assert');

const uri = process.env.MONGO_URI // uri of mongodb in our computer for connection

// Make new client / connection
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();
// Make connection to mongodb
module.exports = client;

// module.exports = (function() {
//  const client = MongoClient.connect(uri, {
//    useNewUrlParser: true,
//    useUnifiedTopology: true ,
//  });
//
//   //const db = client.db("academy");
//   //return { client, db };
//   return client;
// })();

// MongoClient.connect(uri,  { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
//   assert.equal(null, err);
//   client.close();
// });
//
// module.exports = MongoClient;
