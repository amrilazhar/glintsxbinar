const validator = require("validator"); // Import validator
const client = require("../../models"); // Import connection
const { ObjectId } = require("mongodb"); // Import ObjectId

module.exports.test = async (req, res, next) => {
  if (!validator.isAlpha(req.body.nama)) {
    return res.status(400).json({ message: "error namanya" });
  }
  next();
};
