require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
const express = require("express"); // Import express
const app = express(); // Make express app
const myRoutes = require("./routes/myRoute"); // Import routes for transaksi

//Set body parser for HTTP post operation
app.use(express.json()); // support json encoded bodies
app.use(
  express.urlencoded({
    extended: true,
  })
); // support encoded bodies

// pass the connection
let mC =require("./models");
app.set("mongoCon",mC );

app.use("/start", myRoutes); // If accessing localhost:3000/transaksi/*, it will go to transaksiRoutes variable
app.listen(3000); // make port 3000 for this app
