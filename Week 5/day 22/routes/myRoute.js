const express = require("express"); // Import express
const router = express.Router(); // Make express router
const myController = require("../controllers/myController.js"); // Import transaksiController from controllers directory
const myValidator = require("../middlewares/validators/myValidator.js"); // Import transaksiValidator
const { imageUpload } = require("../middlewares/uploads/imageUpload");

function connectToMongo(req,res,next){
  req.app.conMongo = req.app.get('mongoCon');
  next();
}

router.get("/getStudents", connectToMongo, myController.getStudents);
router.get("/getClass", connectToMongo,myController.getClass);
router.get("/getStudent/:id", connectToMongo,myController.getStudent);
router.get("/getClass/:id", connectToMongo,myController.getOneClass);
router.post("/insertStudent",connectToMongo, myController.insertStudent);
// router.put("/update/:id", myValidator.update, myValidator.update);
router.delete("/deleteStudent/:id", connectToMongo, myController.deleteStudent);
router.post("/testBody", connectToMongo, imageUpload, myValidator.test, myController.test)

module.exports = router; // Export router
