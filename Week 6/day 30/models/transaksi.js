const mongoose = require("mongoose");
const mongoose_delete = require("mongoose-delete");

const TransaksiSchema = new mongoose.Schema(
  {
    barang: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      get: getBarang,
    },
    pelanggan: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
      get: getPelanggan,
    },
    jumlah: {
      type: Number,
      required: true,
    },
    total: {
      type: mongoose.Schema.Types.Decimal128,
      required: true,
    },
    faktur: {
      type: String,
      default: null,
      get: getImage,
      required: false,
    },
    createdBy: {
      type: mongoose.Schema.ObjectId,
      default: null,
      required: false,
    },
    updatedBy: {
      type: mongoose.Schema.ObjectId,
      default: null,
      required: false,
    },
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
    toJSON: { getters: true },
  }
);

function getImage(image) {
  return image && `/images/${image}`;
}

function getPelanggan(data) {
  // put the data on tempeorary, so we can manipulate it easily
  let temp = Object.assign({}, data);
  if (temp.photo) {
    temp.photo = "/images/".concat(temp.photo);
    return temp;
  } else return data;
}

function getBarang(data) {
  // put the data on tempeorary, so we can manipulate it easily
  let temp = Object.assign({}, data);
  if (temp.image) {
    temp.image = "/images/".concat(temp.image);
    return temp;
  } else return data;
}

TransaksiSchema.plugin(mongoose_delete, {
  overrideMethods: "all",
});

module.exports = transaksi = mongoose.model(
  "transaksi",
  TransaksiSchema,
  "transaksi"
);
