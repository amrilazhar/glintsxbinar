const mongoose = require("mongoose");
const mongoose_delete = require('mongoose-delete');

const BarangSchema = new mongoose.Schema({
  nama: {
    type: String,
    required: true,
    unique: true
  },
  harga: {
    type: Number,
    required: true
  },
  pemasok: {
    type: mongoose.Schema.ObjectId,
    ref: "pemasok",
    required: true,
  },
  image: {
    type: String,
    default: null,
    get: getImage,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  },
  toJSON: { getters: true },
});

function getImage(image) {
  return image && `/images/${image}`;
}

BarangSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = barang = mongoose.model('barang', BarangSchema, 'barang');
