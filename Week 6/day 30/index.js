require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
// Express
const express = require("express");
const app = express();
const transaksiRoutes = require("./routes/transaksiRoute.js");
const authRoutes = require("./routes/authRoute.js");
const fileUpload = require("express-fileupload")

//Set body parser for HTTP post operation
app.use(express.json()); // support json encoded bodies
app.use(
  express.urlencoded({
    extended: true,
  })
); // support encoded bodies

app.use(fileUpload());
//set static assets to public directory
app.use(express.static("public"));


app.use("/transaksi", transaksiRoutes); // if accessing localhost:3000/transaksi/* we will go to transaksiRoutes
app.use("/auth", authRoutes);

if (process.env.NODE_ENV !== "test") {
  let PORT = 3000;
  app.listen(PORT, () => console.log("server running on http://localhost:",PORT));
} else module.exports = app;
