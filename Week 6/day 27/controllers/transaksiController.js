const { barang, pelanggan, pemasok, transaksi } = require("../models");
const mongoose = require("mongoose");

class TransaksiController {
  // async unAuthorized(req, res) {
  //   try {
  //     return res.status(404).json({ message: "unAuthorized Access, you must be an ADMIN, ... back to login page" });
  //   } catch (e) {
  //     console.log(e);
  //     return res.status(500).json({ messange: "Internal server Error" });
  //   }
  // }

  async getAll(req, res) {
    try {
      let findTrans = await transaksi.find();

      if (findTrans.length === 0) {
        res.status(404).json({ message: "transaksi not found" });
      } else {
        res.status(200).json({ data: findTrans });
      }
    } catch (e) {
      console.log(e);
      return res.status(500).json({ messange: "Internal server Error" });
    }
  }

  async getOne(req, res) {
    try {
      let findTrans = await transaksi.findOne({ _id: req.params.id });

      console.log(findTrans);
      res.status(200).json({
        data: findTrans,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({ messange: "Internal server Error" });
    }
  }

  //created
  async create(req, res) {
    try {
      let BarangIsValid = mongoose.Types.ObjectId.isValid(req.body.id_barang);
      let PelangganIsValid = mongoose.Types.ObjectId.isValid(
        req.body.id_pelanggan
      );

      if (!BarangIsValid || !PelangganIsValid) {
        return res
          .status(404)
          .json({ message: "Barang / Pelanggan id is invalid" });
      }

      let findData = await Promise.all([
        barang.findOne({ _id: req.body.id_barang }),
        pelanggan.findOne({ _id: req.body.id_pelanggan }),
      ]);

      if (!findData[0] || !findData[1]) {
        return res
          .status(404)
          .json({ message: "Barang / Pelanggan not Found" });
      }

      let total = eval(findData[0].harga * req.body.jumlah);
      let data = await transaksi.create({
        barang: findData[0],
        pelanggan: findData[1],
        jumlah: req.body.jumlah,
        total,
        faktur: req.body.faktur,
        createdBy : req.user.id && req.user.id,
        updatedBy : req.user.id && req.user.id,
      });

      if (!data) {
        res.status(402).json({ message: "error create" });
      } else res.status(200).json({ message: "success", data });
    } catch (e) {
      console.log(e);
      return res.status(500).json({ messange: "Internal server Error" });
    }
  }

  //created
  async update(req, res) {
    try {
      let BarangIsValid = mongoose.Types.ObjectId.isValid(req.body.id_barang);
      let PelangganIsValid = mongoose.Types.ObjectId.isValid(
        req.body.id_pelanggan
      );
      let TransaksiIsValid = mongoose.Types.ObjectId.isValid(
        req.body.id_pelanggan
      );

      if (!BarangIsValid || !PelangganIsValid || !TransaksiIsValid) {
        return res
          .status(404)
          .json({ message: "Barang / Pelanggan / Transaksi id is invalid" });
      }

      let findData = await Promise.all([
        barang.findOne({ _id: req.body.id_barang }),
        transaksi.findOne({ _id: req.params.id }),
        pelanggan.findOne({ _id: req.body.id_pelanggan }),
      ]);
      console.log(findData[0]);
      if (!findData[0] || !findData[1]) {
        return res
          .status(404)
          .json({ message: "Barang / Pelanggan not Found" });
      }

      let total = eval(findData[0].harga * req.body.jumlah);
      let data = transaksi.update(
        { _id: req.params.id },
        {
          barang: findData[0],
          pelanggan: findData[1],
          jumlah: req.body.jumlah,
          total,
          faktur: req.body.faktur,
          updatedBy : req.user.id && req.user.id,
        }
      );
    } catch (e) {
      console.log(e);
      return res.status(500).json({ messange: "Internal server Error" });
    }
  }
}

module.exports = new TransaksiController();
