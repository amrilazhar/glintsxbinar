const mongoose = require("mongoose");

const uri = process.env.MONGO_URI;

mongoose
  .connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true, //use to make unique data Types
    useFindAndModify: false, //usefindAndUpdate instead of findAndModify
  })
  .then(() => console.log("database connected"))
  .catch((e) => console.log(e));

const barang = require("./barang.js");
const pelanggan = require("./pelanggan.js");
const pemasok = require("./pemasok");
const transaksi = require("./transaksi.js");
const user = require("./users.js");

module.exports = { barang, pelanggan, pemasok, transaksi, user };
