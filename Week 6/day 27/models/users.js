const mongoose = require("mongoose");
const mongoose_delete = require("mongoose-delete");
const bcrypt = require("bcrypt"); // Import bcrypt

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: false,
      set: encryptPwd,
    },
    role: {
      type: String,
      required: true,
      default: "user",
    },
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
  }
);

function encryptPwd(password) {
  return bcrypt.hashSync(password, 13);
}
UserSchema.plugin(mongoose_delete, { overrideMethods: "all" });

module.exports = mongoose.model("user", UserSchema, "user");
