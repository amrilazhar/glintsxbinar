const express = require("express");

const transaksiController = require("../controllers/transaksiController");

const router = express.Router();

// import for auth needs
const passport = require("passport");
const { isAdmin, isUser } = require("../middlewares/auth/");

// if failed to authorize, redirect to this page
//router.get("/unauthorized", transaksiController.unAuthorized);

router.get("/", isUser, transaksiController.getAll);
router.get("/:id", isUser, transaksiController.getOne);
router.post("/create", isAdmin, transaksiController.create);
router.put("/update/:id", isAdmin, transaksiController.update);


module.exports = router;
